<?php

namespace App\Http\Controllers\API;

use App\Models\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DateTime;

class DataBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listBarang = Barang::select('id', 'kode_barang', 'nama_barang', 'deskripsi', 'stok_barang', 'harga_barang')
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $listBarang], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required|unique:barang,kode_barang',
            'nama_barang' => 'required',
            'stok_barang' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        #ELOQUENT
        $barang = new Barang();
        $barang->_token = $request->input('_token');
        $barang->kode_barang = $request->input('kode_barang');
        $barang->nama_barang = $request->input('nama_barang');
        $barang->deskripsi = $request->input('deskripsi');
        $barang->stok_barang = $request->input('stok_barang');
        $barang->harga_barang = $request->input('harga_barang');
        $barang->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::select('id', 'kode_barang', 'nama_barang', 'deskripsi', 'stok_barang', 'harga_barang')
            ->where('id', '=', $id)
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data Found', 'data' => $barang], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'nama_barang' => 'required',
            'stok_barang' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        #ELOQUENT
        $barang = Barang::find($id);
        $barang->kode_barang = $request->input('kode_barang');
        $barang->nama_barang = $request->input('nama_barang');
        $barang->deskripsi = $request->input('deskripsi');
        $barang->stok_barang = $request->input('stok_barang');
        $barang->harga_barang = $request->input('harga_barang');
        $barang->_method = $request->input('_method');
        $barang->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $now = new DateTime();

        #ELOQUENT
        $barang = Barang::find($id);
        if (!$barang) {
            return response()->json(['error_code' => '402', 'message' => 'Data Not Found', 'data' => 'data not found'], 400);
        }
        $barang->deleted_at = $now;
        $barang->save();

        return response()->json(['message' => 'Delete Data Success', 'data' => 'berhasil dihapus'], 201);
    }
}