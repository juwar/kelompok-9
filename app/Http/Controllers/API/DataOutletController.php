<?php

namespace App\Http\Controllers\API;

use App\Models\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DateTime;

class DataOutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listOutlet = Outlet::select('id', 'kode_outlet', 'nama_outlet', 'jumlah_display', 'visit_datetime')
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $listOutlet], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_outlet' => 'required|unique:outlet,kode_outlet',
            'nama_outlet' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        #ELOQUENT
        $outlet = new Outlet();
        $outlet->_token = $request->input('_token');
        $outlet->kode_outlet = $request->input('kode_outlet');
        $outlet->nama_outlet = $request->input('nama_outlet');
        $outlet->jumlah_display = $request->input('jumlah_display');
        $outlet->visit_datetime = $request->input('visit_datetime');
        $outlet->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outlet = Outlet::select('id', 'kode_outlet', 'nama_outlet', 'jumlah_display', 'visit_datetime')
            ->where('id', '=', $id)
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data Found', 'data' => $outlet], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'nama_outlet' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        $outlet = Outlet::find($id);
        $outlet->kode_outlet = $request->input('kode_outlet');
        $outlet->nama_outlet = $request->input('nama_outlet');
        $outlet->jumlah_display = $request->input('jumlah_display');
        $outlet->visit_datetime = $request->input('visit_datetime');
        $outlet->_method = $request->input('_method');
        $outlet->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $now = new DateTime();

        #ELOQUENT
        $outlet = Outlet::find($id);
        if (!$outlet) {
            return response()->json(['error_code' => '402', 'message' => 'Data Not Found', 'data' => 'data not found'], 400);
        }
        $outlet->deleted_at = $now;
        $outlet->save();

        return response()->json(['message' => 'Delete Data Success', 'data' => 'berhasil dihapus'], 201);
    }
}