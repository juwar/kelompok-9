<?php

namespace App\Http\Controllers\API;

use App\Models\Transaksi;
use App\Models\Barang;
use App\Models\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Illuminate\Support\Facades\Log;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'id_barang' => 'required',
            'id_outlet' => 'required',
            'visit_datetime' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        #ELOQUENT
        $now = new DateTime();
        $barang = Barang::find($request->input('id_barang'));
        $outlet = Outlet::find($request->input('id_outlet'));

        $transaksi = new Transaksi();
        $transaksi->_token = $request->input('_token');
        $transaksi->id_sales = Auth::user()->id;
        $transaksi->id_barang = $request->input('id_barang');
        $transaksi->jumlah_stok = $barang->stok_barang;
        $transaksi->id_outlet = $request->input('id_outlet');
        $transaksi->jumlah_display = $outlet->jumlah_display;
        $transaksi->visit_datetime = $request->input('visit_datetime');
        $transaksi->save();


        $outlet->_token = $request->input('_token');
        $outlet->visit_datetime = $request->input('visit_datetime');
        $outlet->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'id_barang' => 'required',
            'id_outlet' => 'required',
            'visit_datetime' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['error_code' => '401', 'message' => 'Data Already Exist', 'data' => $messages], 400);
        }

        #ELOQUENT
        $now = new DateTime();
        $barang = Barang::find($request->input('id_barang'));
        $outlet = Outlet::find($request->input('id_outlet'));

        $transaksi = Transaksi::find($id);
        $transaksi->_token = $request->input('_token');
        $transaksi->id_sales = Auth::user()->id;
        $transaksi->id_barang = $request->input('id_barang');
        $transaksi->jumlah_stok = $barang->stok_barang;
        $transaksi->id_outlet = $request->input('id_outlet');
        $transaksi->jumlah_display = $outlet->jumlah_display;
        $transaksi->visit_datetime = $request->input('visit_datetime');
        $transaksi->save();


        $outlet->_token = $request->input('_token');
        $outlet->visit_datetime = $request->input('visit_datetime');
        $outlet->save();

        return response()->json(['message' => 'Submit Success', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $now = new DateTime();

        #ELOQUENT
        $barang = Transaksi::find($id);
        if (!$barang) {
            return response()->json(['error_code' => '402', 'message' => 'Data Not Found', 'data' => 'data not found'], 400);
        }
        $barang->deleted_at = $now;
        $barang->save();

        return response()->json(['message' => 'Delete Data Success', 'data' => 'berhasil dihapus'], 201);
    }
}