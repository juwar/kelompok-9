<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $barang = Barang::where('deleted_at', '=', null)
                ->where('kode_barang', 'LIKE', "%$keyword%")
                ->orWhere('nama_barang', 'LIKE', "%$keyword%")
                ->orWhere('deskripsi', 'LIKE', "%$keyword%")
                ->orWhere('stok_barang', 'LIKE', "%$keyword%")
                ->orWhere('harga_barang', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $barang = Barang::where('deleted_at', '=', null)->paginate($perPage);
        }

        return view('barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $responses = app('App\Http\Controllers\API\DataBarangController')->store($request)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('barang')->with($flash_message, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $barang = Barang::findOrFail($id);

        return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $barang = Barang::findOrFail($id);

        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $responses = app('App\Http\Controllers\API\DataBarangController')->update($request, $id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('barang')->with($flash_message, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $responses = app('App\Http\Controllers\API\DataBarangController')->destroy($id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('barang')->with($flash_message, $message);
    }
}