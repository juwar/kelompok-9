<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Outlet;
use Illuminate\Http\Request;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $outlet = Outlet::where('deleted_at', '=', null)
                ->where('kode_outlet', 'LIKE', "%$keyword%")
                ->orWhere('nama_outlet', 'LIKE', "%$keyword%")
                ->orWhere('jumlah_display', 'LIKE', "%$keyword%")
                ->orWhere('visit_datetime', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $outlet = Outlet::where('deleted_at', '=', null)->paginate($perPage);
        }

        return view('outlet.outlet.index', compact('outlet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('outlet.outlet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $responses = app('App\Http\Controllers\API\DataOutletController')->store($request)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('outlet')->with($flash_message, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $outlet = Outlet::findOrFail($id);

        return view('outlet.outlet.show', compact('outlet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $outlet = Outlet::findOrFail($id);

        return view('outlet.outlet.edit', compact('outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $responses = app('App\Http\Controllers\API\DataOutletController')->update($request, $id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('outlet')->with($flash_message, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $responses = app('App\Http\Controllers\API\DataOutletController')->destroy($id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('outlet')->with($flash_message, $message);
    }
}