<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Barang;
use App\Models\Outlet;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $transaksi = Transaksi::where('deleted_at', '=', null)
                ->where('id_sales', 'LIKE', "%$keyword%")
                ->orWhere('id_barang', 'LIKE', "%$keyword%")
                ->orWhere('id_outlet', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            // $transaksi = Transaksi::with(['barang'])->where('deleted_at', '=', null)->paginate($perPage);
            $transaksi = DB::table('transaksi')
                ->join('users', 'transaksi.id_sales', '=', 'users.id')
                ->join('barang', 'transaksi.id_barang', '=', 'barang.id')
                ->join('outlet', 'transaksi.id_outlet', '=', 'outlet.id')
                ->select('transaksi.*', 'users.name', 'barang.nama_barang', 'outlet.nama_outlet')
                ->where('transaksi.deleted_at', '=', null)
                ->paginate($perPage);
        }
        Log::info('barang', [$transaksi]);

        return view('transaksi.transaksi.index', compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $list_barang = Barang::where('deleted_at', '=', null)->pluck('nama_barang', 'id');
        $list_outlet = Outlet::where('deleted_at', '=', null)->pluck('nama_outlet', 'id');
        return view('transaksi.transaksi.create', compact('list_barang', 'list_outlet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $responses = app('App\Http\Controllers\API\TransaksiController')->store($request)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('transaksi')->with($flash_message, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // $transaksi = Transaksi::findOrFail($id);
        $transaksi = DB::table('transaksi')
            ->join('users', 'transaksi.id_sales', '=', 'users.id')
            ->join('barang', 'transaksi.id_barang', '=', 'barang.id')
            ->join('outlet', 'transaksi.id_outlet', '=', 'outlet.id')
            ->select('transaksi.*', 'users.name', 'barang.nama_barang', 'outlet.nama_outlet')
            ->where('transaksi.id', '=', $id)
            ->where('transaksi.deleted_at', '=', null)
            ->first();
        Log::info('barang', [$transaksi]);

        return view('transaksi.transaksi.show', compact('transaksi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $transaksi = Transaksi::findOrFail($id);
        $list_barang = Barang::where('deleted_at', '=', null)->pluck('nama_barang', 'id');
        $list_outlet = Outlet::where('deleted_at', '=', null)->pluck('nama_outlet', 'id');

        return view('transaksi.transaksi.edit', compact('transaksi', 'list_barang', 'list_outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $responses = app('App\Http\Controllers\API\TransaksiController')->update($request, $id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('transaksi')->with($flash_message, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $responses = app('App\Http\Controllers\API\TransaksiController')->destroy($id)->getData();
        $message = $responses->message;
        $flash_message = isset($responses->error_code) ? 'flash_message_error' : 'flash_message';

        return redirect('transaksi')->with($flash_message, $message);
    }
}