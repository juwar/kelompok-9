<div class="form-group{{ $errors->has('kode_barang') ? 'has-error' : ''}}">
    {!! Form::label('kode_barang', 'Kode Barang', ['class' => 'control-label']) !!}
    {!! Form::text('kode_barang', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('kode_barang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nama_barang') ? 'has-error' : ''}}">
    {!! Form::label('nama_barang', 'Nama Barang', ['class' => 'control-label']) !!}
    {!! Form::text('nama_barang', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nama_barang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('deskripsi') ? 'has-error' : ''}}">
    {!! Form::label('deskripsi', 'Deskripsi', ['class' => 'control-label']) !!}
    {!! Form::text('deskripsi', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('stok_barang') ? 'has-error' : ''}}">
    {!! Form::label('stok_barang', 'Stok Barang', ['class' => 'control-label']) !!}
    {!! Form::text('stok_barang', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('stok_barang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('harga_barang') ? 'has-error' : ''}}">
    {!! Form::label('harga_barang', 'Harga Barang', ['class' => 'control-label']) !!}
    {!! Form::text('harga_barang', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('harga_barang', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
