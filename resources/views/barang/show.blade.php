@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('sales.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Barang {{ $barang->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/barang') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/barang/' . $barang->id . '/edit') }}" title="Edit Barang"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['barang', $barang->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Barang',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $barang->id }}</td>
                                    </tr>
                                    <tr><th> Kode Barang </th><td> {{ $barang->kode_barang }} </td></tr><tr><th> Nama Barang </th><td> {{ $barang->nama_barang }} </td></tr><tr><th> Deskripsi </th><td> {{ $barang->deskripsi }} </td></tr><tr><th> Stok Barang </th><td> {{ $barang->stok_barang }} </td></tr><tr><th> Harga </th><td> {{ $barang->harga_barang }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
