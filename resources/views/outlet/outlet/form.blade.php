<div class="form-group{{ $errors->has('kode_outlet') ? 'has-error' : ''}}">
    {!! Form::label('kode_outlet', 'Kode Outlet', ['class' => 'control-label']) !!}
    {!! Form::text('kode_outlet', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('kode_outlet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nama_outlet') ? 'has-error' : ''}}">
    {!! Form::label('nama_outlet', 'Nama Outlet', ['class' => 'control-label']) !!}
    {!! Form::text('nama_outlet', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nama_outlet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('jumlah_display') ? 'has-error' : ''}}">
    {!! Form::label('jumlah_display', 'Jumlah Display', ['class' => 'control-label']) !!}
    {!! Form::number('jumlah_display', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('jumlah_display', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('visit_datetime') ? 'has-error' : ''}}">
    {!! Form::label('visit_datetime', 'Visit Datetime', ['class' => 'control-label']) !!}
    {!! Form::input('datetime-local', 'visit_datetime', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('visit_datetime', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
