@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('sales.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Outlet</div>
                    <div class="card-body">
                        <a href="{{ url('/outlet/create') }}" class="btn btn-success btn-sm" title="Add New Outlet">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/outlet', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Kode Outlet</th><th>Nama Outlet</th><th>Jumlah Display</th><th>Visit</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($outlet as $key=>$item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->kode_outlet }}</td><td>{{ $item->nama_outlet }}</td><td>{{ $item->jumlah_display }}</td><td>{{ \Carbon\Carbon::parse($item->visit_datetime) }}</td>
                                        <td>
                                            <a href="{{ url('/outlet/' . $item->id) }}" title="View Outlet"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/outlet/' . $item->id . '/edit') }}" title="Edit Outlet"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/outlet', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Outlet',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $outlet->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
