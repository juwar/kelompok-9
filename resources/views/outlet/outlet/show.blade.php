@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('sales.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Outlet {{ $outlet->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/outlet') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/outlet/' . $outlet->id . '/edit') }}" title="Edit Outlet"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['outlet', $outlet->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Outlet',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $outlet->id }}</td>
                                    </tr>
                                    <tr><th> Kode Outlet </th><td> {{ $outlet->kode_outlet }} </td></tr><tr><th> Nama Outlet </th><td> {{ $outlet->nama_outlet }} </td></tr><tr><th> Jumlah Display </th><td> {{ $outlet->jumlah_display }} </td></tr><tr><th> Visit </th><td> {{ \Carbon\Carbon::parse($outlet->visit_datetime) }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
