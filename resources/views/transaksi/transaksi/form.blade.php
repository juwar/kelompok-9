
<div class="form-group{{ $errors->has('id_barang') ? 'has-error' : ''}}">
    {!! Form::label('id_barang', 'Barang', ['class' => 'control-label']) !!}
    {!! Form::select('id_barang', $list_barang, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('id_barang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('id_outlet') ? 'has-error' : ''}}">
    {!! Form::label('id_outlet', 'Outlet', ['class' => 'control-label']) !!}
    {!! Form::select('id_outlet', $list_outlet, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('id_outlet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('visit_datetime') ? 'has-error' : ''}}">
    {!! Form::label('visit_datetime', 'Visit Datetime', ['class' => 'control-label']) !!}
    {!! Form::input('datetime-local', 'visit_datetime', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('visit_datetime', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
