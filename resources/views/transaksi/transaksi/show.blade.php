@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('sales.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Transaksi {{ $transaksi->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/transaksi') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/transaksi/' . $transaksi->id . '/edit') }}" title="Edit Transaksi"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['transaksi', $transaksi->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Transaksi',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $transaksi->id }}</td>
                                    </tr>
                                    <tr><th> Sales </th><td> {{ $transaksi->name }} </td></tr><tr><th> Barang </th><td> {{ $transaksi->nama_barang }} </td></tr><tr><th> Outlet </th><td> {{ $transaksi->nama_outlet }} </td></tr><tr><th> Stock </th><td> {{ $transaksi->jumlah_stok }} </td></tr><tr><th> Jumlah Display </th><td> {{ $transaksi->jumlah_display }} </td></tr><tr><th> Visit </th><td> {{ $transaksi->visit_datetime }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
