<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('list-barang', 'API\DataBarangController@index');
Route::post('add-barang', 'API\DataBarangController@store');
Route::get('find-barang/{id}', 'API\DataBarangController@show');
Route::post('update-barang/{id}', 'API\DataBarangController@update');
Route::post('delete-barang/{id}', 'API\DataBarangController@destroy');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });