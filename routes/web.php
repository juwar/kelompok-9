<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', function () {
    if (Auth::check() && Auth::user()->hasRole('admin')) {
        // Do admin stuff here
        return redirect('/admin');
    } else if (Auth::check() && Auth::user()->hasRole('sales')) {
        // Do admin stuff here
        return redirect('/transaksi');
    } else {
        // Do nothing
        return redirect('/login');
    }
    Log::info('Admin', [Auth::user()->hasRole('admin')]);
});

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'can:all']], function () {
    Route::get('/', ['uses' => 'AdminController@index']);
    Route::resource('/roles', 'RolesController');
    Route::resource('/permissions', 'PermissionsController');
    Route::resource('/users', 'UsersController');
    Route::resource('/pages', 'PagesController');
    Route::resource('/activitylogs', 'ActivityLogsController')->only([
        'index',
        'show',
        'destroy'
    ]);
    Route::resource('/settings', 'SettingsController');
    Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});
Route::group(['namespace' => 'Admin', 'prefix' => 'sales', 'middleware' => ['auth', 'can:sales']], function () {
    Route::get('/', ['uses' => 'SalesController@index']);
});
Route::resource('barang', 'Web\BarangController');
Route::resource('outlet', 'Web\OutletController');
Route::resource('outlet', 'Web\OutletController');
Route::resource('transaksi', 'Web\TransaksiController');